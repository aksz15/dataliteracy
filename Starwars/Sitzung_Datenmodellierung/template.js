const R = require('ramda')
const fs = require('fs')

// Liest eine Datei ein; der Pfad wird der Ramda-Pipe unten übergeben.
// Pfad (String) -> Konfig-Objekt -> String
const readFile = path => fs.readFileSync(path, { encoding: 'utf-8'}); 
// Schreibt Daten in eine Datei.
// Pfad (String) -> String -> Konfig-Objekt -> void
const writeFile = data => fs.writeFileSync('./output/out.json', data, { encoding: 'utf-8'}); 
// Gibt das aktuelle Datum aus 
// --> String
const now = () => {
        var today = new Date();
        var date = today.getFullYear()+'-'+(today.getMonth()+1)+'-'+today.getDate();
        return date;
}

// Reducer
// Objekt -> String -> Objekt
const reduceMetadata = (acc,curr) => {
    switch (curr.trim().slice(1,2)) {
        case 't':
            acc.title = curr.trim().slice(2).trim();
            break;
        case 's':
            acc.subtitle = curr.trim().slice(2).trim();
            break;
        case 'm':
            acc.movie = curr.trim().slice(2).trim();
            break;
        case 'a':
            acc.author = curr.trim().slice(2).trim();
            break;
        case 'v':
            acc.version = curr.trim().slice(2).trim();
            break;
        case 'd':
            acc.date = curr.trim().slice(2).trim();
            break;
        case 'c':
            acc.company = curr.trim().slice(2).trim();
            break;
        default:
            break;
    }

    return acc;
};

// Erstellt das grundlegende JSON-Objekt
// Array -> Objekt
const baseObject = arr => {
    return {
        metadata: arr[0], 
        content: arr[1] 
    }
}; 

// Setzt die Metadaten des JSON-Objekts auf Basis der Text-Datei
// Objekt -> Objekt
const setMetadata = obj => {
    var { metadata, content } = obj;
    
    return {
        metadata: metadata.split('\n')
                          .filter(i => { return i != ''; })
                          .reduce(reduceMetadata, {}),
        content
    };
}

// Setzt die auf die Prozessierung bezogenen Metadaten
// Objekt -> Objekt
const setFileMetadata = obj => {
    const { metadata, content } = obj;

    return {
        metadata: {
            script: metadata,
            file: {
                cruser: [
                    {
                        name: '' // hier ein Objekt pro Team-Mitglied setzen
                    }
                ],
                crdate: now()
            },
        },
        content,
    };
}

// String -> Pipe
R.pipe(
    readFile, // String -> String
    R.split('###        \n'), // String -> String -> Array
    baseObject, // Array -> Objekt
    setMetadata, // Objekt -> Objekt
    setFileMetadata, // Objekt -> Objekt
    console.log, // Objekt -> void
    //JSON.stringify,
    //writeFile
)('./input/IV.txt')