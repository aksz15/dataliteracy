const R = require('ramda')
const fs = require('fs')

const readFile = path => fs.readFileSync(path, { encoding: 'utf-8'});
const writeFile = data => fs.writeFileSync('./output/out.json', data, { encoding: 'utf-8'});
const now = () => {
        var today = new Date();
        var date = today.getFullYear()+'-'+(today.getMonth()+1)+'-'+today.getDate();
        return date;
}

const reduceMetadata = (acc,curr) => {
    switch (curr.trim().slice(1,2)) {
        case 't':
            acc.title = curr.trim().slice(2).trim();
            break;
        case 's':
            acc.subtitle = curr.trim().slice(2).trim();
            break;
        case 'm':
            acc.movie = curr.trim().slice(2).trim();
            break;
        case 'a':
            acc.author = curr.trim().slice(2).trim();
            break;
        case 'v':
            acc.version = curr.trim().slice(2).trim();
            break;
        case 'd':
            acc.date = curr.trim().slice(2).trim();
            break;
        case 'c':
            acc.company = curr.trim().slice(2).trim();
            break;
        default:
            break;
    }

    return acc;
};

const setScenes = (curr,index,array) => {
    try {
        var str = array[index+1];
        var match = str.match(/^[ ](.*?)\n\n(.*)/);
        var location = match[1];
        var content = str.replace(/^[ ].*?\n\n/, '');
        var roles = content.split('\n').filter(i => { return i.match(/^\#\s+[A-Z \-]+$/); }).map(i => { return i.slice(1).trim(); });
    } catch (error) {
        var location = 'regex_error';
        var content = str;
    }
    
    switch (curr.trim()) {
        case 'INT.':
            return { type: 'internal', location: location, content: content, roles: Array.from(new Set(roles))}
            break;
        case 'EXT.':
            return { type: 'external', location: location, content: content, roles: Array.from(new Set(roles))}
            break;
        default:
            return ''; 
            break;
    }
};


const baseObject = arr => {
    return {
        metadata: arr[0], 
        content: arr[1] 
    }
}; 

const setMetadata = obj => {
    var { metadata, content } = obj;
    
    return {
        metadata: metadata.split('\n')
                          .filter(i => { return i != ''; })
                          .reduce(reduceMetadata, {}),
        content
    };
}

const setContent = (curr,index,array) => {
    try {
        var content = curr.content.split('\n\n').filter(i => { return i != ''; }).map((i) => {
            if (i.slice(0,1) === '#'){
                const [ speaker, ...rest ] = i.slice(1).trim().split('\n');
                return {
                    type: 'speech',
                    speaker: speaker,
                    content: rest.map(i => { return i.trim(); }).join(' ')
                };
            } else {
                return {
                    type: 'description',
                    content: i.trim().split('\n').map(i => { return i.trim(); }).join(' ')
                };
            }
        });

    } catch (error) {
        var content = 'ERROR';
    }
    

    return {
        ...curr,
        content
    } ;
};

const setContentdata = obj => {
    const { metadata, content } = obj;

    return { 
        metadata, 
        content: content.split(/^\#.*?(INT\.|EXT\.)/m)
                        .filter(i => { return i != ''; })
                        .map(setScenes)
                        .filter(i => {return i != ''; })
                        .map(setContent)
    };
};



const setFileMetadata = obj => {
    const { metadata, content } = obj;

    return {
        metadata: {
            script: metadata,
            file: {
                cruser: [
                    {
                        name: 'Max Grüntgens'
                    }
                ],
                crdate: now()
            },
        },
        content,
    };
}

R.pipe(
    readFile,
    R.split('###        \n'),
    baseObject,
    setMetadata,
    setContentdata,
    setFileMetadata,
    //console.log,
    JSON.stringify,
    writeFile
)('./input/IV.txt')

//#               #