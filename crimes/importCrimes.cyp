CREATE INDEX domesticIndex IF NOT EXISTS FOR (d: Domestic) ON (d.type);
CREATE INDEX fbiIndex IF NOT EXISTS FOR (fbi: FBICode) ON (fbi.code);
CREATE INDEX yearIndex IF NOT EXISTS FOR (y: Year) ON (y.year);
CREATE INDEX primTypeIndex IF NOT EXISTS FOR (p: PrimaryType) ON (p.type);
CREATE INDEX beatIndex IF NOT EXISTS FOR (b: Beat) ON (b.name);
CREATE INDEX districtIndex IF NOT EXISTS FOR (di: District) ON (di.name);
CREATE INDEX wardIndex IF NOT EXISTS FOR (w: Ward) ON (w.name);
CREATE INDEX commAreaIndex IF NOT EXISTS FOR (ca: CommunityArea) ON (ca.name);
CREATE INDEX block IF NOT EXISTS FOR (bl: Block) ON (bl.name);
CALL apoc.periodic.iterate('load csv with headers from
// "http://datascience.mni.thm.de/~kuczera/dataLiteracy/crimes1000.csv"
"http://datascience.mni.thm.de/~kuczera/dataLiteracy/crimes.csv"
as row return row',
'create (c: Case {
id: row.ID,
caseNum: row.CaseNumber,
day: split(row.Date," ")[0],
time: (split(row.Date, " ")[1] + " " + split(row.Date, " ")[2]),
block: row.Block,
iucr: row.IUCR,
primaryType: row.PrimaryType,
description: row.Description,
locDescription: row.LocationDescription,
arrest: row.Arrest,
domestic: row.Domestic,
ward: row.Ward,
xCoor: toInteger(row.XCoordinate),
yCoor: toInteger(row.YCoordinate),
updatedOn: row.UpdatedOn,
lat: toFLoat(row.Latitude),
lon: toFLoat(row.Longitude),
fbiCode: row.FBICode,
beat: row.Beat,
district: row.District,
communityArea: row.CommunityArea,
year: toInteger(row.Year),
location: row.Location
})', {batchSize:1000});

CALL apoc.periodic.iterate('
match (c:Case)
where c.arrest is not null return c',
'merge (a: Arrested {type: c.arrest})
merge (c)-[:got_arrested]->(a)' ,{batchSize:1000});
CALL apoc.periodic.iterate('
match (c:Case)
where c.domestic is not null return c',
'merge (d: Domestic {type: c.domestic})
merge (c)-[:was_domestic]->(d)', {batchSize:1000});
CALL apoc.periodic.iterate('
match (c:Case)
where c.fbiCode is not null return c',
'merge (fbi: FBICode {code: c.fbiCode})
merge (c)-[:has_Code]->(fbi)', {batchSize:1000});
CALL apoc.periodic.iterate('
match (c:Case)
where c.year is not null return c',
'merge (y: Year {year: c.year})
merge (c)-[:was_in_year]->(y)', {batchSize:1000});

CALL apoc.periodic.iterate('
match (c:Case)
where c.primaryType is not null return c',
'merge (p: PrimaryType {type: c.primaryType})
merge (c)-[:has_primary_type]->(p)', {batchSize:1000});
CALL apoc.periodic.iterate('
match (c:Case)
where c.beat is not null return c',
'merge (b: Beat {name: c.beat})
merge (c)-[:was_in_beat]->(b)', {batchSize:1000});
CALL apoc.periodic.iterate('
match (c:Case)
where c.district is not null return c',
'merge (di: District {name: c.district})
merge (c)-[:was_in_district]->(di)', {batchSize:1000});
CALL apoc.periodic.iterate('
match (c:Case)
where c.ward is not null return c',
'merge (w: Ward {name: c.ward})
merge (c)-[:was_in_ward]->(w)', {batchSize:1000});
CALL apoc.periodic.iterate('
match (c:Case)
where c.communityArea is not null return c',
'merge (ca: CommunityArea {name: c.communityArea})
merge (c)-[:was_in_com_area]->(ca)', {batchSize:1000});
CALL apoc.periodic.iterate('
match (c:Case)
where c.block is not null return c',
'merge (bl: Block {name: c.block})
merge (c)-[:was_in_block]->(bl)', {batchSize:1000});
CALL apoc.periodic.iterate('
match (c:Case) return c',
'WITH [item in split(c.day, "/") | toInteger(item)] AS dateComponents, c
set c.day = date({day: dateComponents[1], month: dateComponents[0], year: dateComponents[2]})', {batchSize:1000});